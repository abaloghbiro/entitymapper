package hu.braininghub.bh06.dao;

import hu.braininghub.bh06.dto.BaseDTO;

public interface BaseDAO<DTO extends BaseDTO> {

	void save(DTO object);

	DTO getById(Long id);
}
