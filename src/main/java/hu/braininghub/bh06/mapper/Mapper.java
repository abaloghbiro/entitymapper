package hu.braininghub.bh06.mapper;

import java.util.List;

public interface Mapper<E, DTO> {

	E transformDto(DTO object);

	DTO transformEntity(E entity);

	List<E> transformDTOs(List<DTO> dtos);

	List<DTO> transformEntities(List<E> entities);

}
