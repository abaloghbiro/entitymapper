package hu.braininghub.bh06.dao;

import hu.braininghub.bh06.dto.BaseDTO;
import hu.braininghub.bh06.mapper.Mapper;
import hu.braininghub.bh06.model.BusinessObject;

public abstract class AbstractJPADAO<E extends BusinessObject, DTO extends BaseDTO> implements BaseDAO<DTO> {

	public void save(DTO object) {
		getEntityMapper().transformDto(object);
	}

	public DTO getById(Long id) {

		E entity = null;

		return getEntityMapper().transformEntity(entity);
	}

	abstract Mapper<E, DTO> getEntityMapper();

}
