package hu.braininghub.bh06.dao;

import hu.braininghub.bh06.dto.CustomerDTO;
import hu.braininghub.bh06.mapper.CustomerMapper;
import hu.braininghub.bh06.mapper.Mapper;
import hu.braininghub.bh06.model.Customer;

public class CustomerDAO extends AbstractJPADAO<Customer, CustomerDTO> {

	@Override
	Mapper<Customer, CustomerDTO> getEntityMapper() {
		return new CustomerMapper();
	}

}
