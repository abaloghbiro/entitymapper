package hu.braininghub.bh06.mapper;

import java.util.List;

import hu.braininghub.bh06.dto.CustomerDTO;
import hu.braininghub.bh06.model.Customer;

public class CustomerMapper implements Mapper<Customer, CustomerDTO> {

	private AddressMapper addressMapper = new AddressMapper();

	public Customer transformDto(CustomerDTO object) {

		Customer c = new Customer();

		c.setAddress(addressMapper.transformDto(object.getAddressDTO()));
		c.setAge(object.getAge());
		c.setName(object.getName());
		return c;
	}

	public CustomerDTO transformEntity(Customer entity) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Customer> transformDTOs(List<CustomerDTO> dtos) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<CustomerDTO> transformEntities(List<Customer> entities) {
		// TODO Auto-generated method stub
		return null;
	}

}
